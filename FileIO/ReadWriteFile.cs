namespace FileIO;

using System.IO;

class ReadWriteFile
{
    private string Path = "";  // Field

    public string FilePath { // Property
        get {return Path;} 
        set {Path = value;} 
    }


    public ReadWriteFile(string APath)
    {
        //this.Path = APath;
        FilePath = APath;
    }

    
    public void WriteToFile(string Text)
    {
        File.WriteAllText(Path, Text + Environment.NewLine);
        File.AppendAllText(Path, Text  + Environment.NewLine);
    }

    public void WriteStream(string Text)
    {
        FileStream f = new FileStream(Path, FileMode.OpenOrCreate);
        StreamWriter s = new StreamWriter(f);
        s.WriteLine(Text);
        s.Close();
        f.Close();
    }

    public void ReadStream()
    {
        FileStream f = new FileStream(Path, FileMode.Open);
        StreamReader sr = new StreamReader(f);
        string line = sr.ReadLine();
        Console.WriteLine(line);
        sr.Close();
        f.Close();
    }

    public void Append(string str)
    {
        if(!File.Exists(Path))
        {
            FileStream fs = File.Create(Path);
            fs.Close();
        }
        File.AppendAllText(Path, str);
    }
}