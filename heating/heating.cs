namespace IoT
{

    public class HeatSystem
    {
        private float temp = 0;
        private string room = "";

        public HeatSystem()
        {

        }

        public HeatSystem(string room)
        {
            this.room = room;
        }

        public HeatSystem(string room, float temp)
        {
            this.room = room;
            this.temp = temp;
        }

        public string GetRoom() {
            return this.room;
        }

        public float getTemp() {
            return this.temp;
        }

        public bool setTemp(float tmp)
        {
            this.temp = tmp;
            return true;
        }

    }

}