namespace Room
{
    class Blinds
    {
        bool open = true;
        private string id = "";

        public Blinds(string id)
        {
            this.id = id;
        }

        public void closeBlinds()
        {
            open = false;
            Console.WriteLine("Blinds closed");
        }

        public void openBlinds()
        {
            open = true;
            Console.WriteLine("Blinds open");
            AirConditioning ac = new AirConditioning();
        }

    }

    public class AirConditioning
    {
        public void turnOn() {
            Console.WriteLine("Aircondition on");
        }

        public static void GetObjType()
        {
            Console.WriteLine("AirConditioning class");
        }

    }
}
