public class DeviceList
{
    private const int MAX_DEVICE = 100;
    private int heatingCounter = 0;
    private IIoTDevice[] heatings;

    private static DeviceList instance;

    // TODO: Make thread safe
    public static DeviceList Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new DeviceList();
            }
            return instance;
        }
    }

    private DeviceList()
    {
        heatings = new IIoTDevice[MAX_DEVICE];
    }
    public void AddHeating(Heating heat)
    {
        this.heatings[this.heatingCounter] = heat;
        this.heatingCounter++;
    }

    public void GetAllHeatings()
    {
        for(int i = 0; i < this.heatingCounter; i++)
        {
            Console.WriteLine(heatings[i].getLabel());
        }
    }

    public void GetAllHeatingsStatus()
    {
        for(int i = 0; i < this.heatingCounter; i++)
        {
            heatings[i].isSwitchedOn();
        }
    }

}