interface IIoTDevice {
    bool switchOn();
    bool switchOff();
    string getLabel();
    void isSwitchedOn();
}