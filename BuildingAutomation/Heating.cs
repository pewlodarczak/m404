public class Heating : IIoTDevice
{
    private bool isOn = false;
    private float threshold = 0;
    private string label = "";

    public Heating(string label)
    {
        this.label = label;
    }

    public float getTemperature() 
    {
        return 23.5F;
    }

    public string getLabel() 
    {
        return this.label;
    }

    public bool setThreashold(float thresh)
    {
        this.threshold = thresh;
        return true;
    }

    public bool switchOn()
    {
        this.isOn = true;
        return this.isOn;
    }

    public bool switchOff()
    {
        this.isOn = false;
        return this.isOn;
    }

    public void isSwitchedOn()
    {
        if(this.isOn) 
        {
            Console.WriteLine(this.label + " is switched on");
        } else
            Console.WriteLine(this.label + " is switched off");
    }
    

}