public delegate void ADelegate(string msg); // declare a delegate

class DelegateDemo
{
    // target method
    public void PrintMessage(string message)
    {
        Console.WriteLine(message);
    }

    public void PrintWords(string message)
    {
        var punctuation = message.Where(Char.IsPunctuation).Distinct().ToArray();
        var words = message.Split().Select(x => x.Trim(punctuation));
        foreach(string s in words)
            Console.WriteLine(s);
    }

    public void InvokeDelegate(ADelegate del) // ADelegate type parameter
    {
        del("'Oh, you can't help that,' said the Cat: 'we're all mad here. I'm mad. You're mad.'");
    }
}