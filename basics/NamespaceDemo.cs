namespace NamespaceName1
{
	class ClassName
	{
		public void DoSomething()
        {
            Console.WriteLine("Do something");
        }
	} 
}

namespace NamespaceName2
{
	class ClassName
	{
        public void DoSomething()
        {
            NamespaceName1.ClassName aClass = new NamespaceName1.ClassName();
            aClass.DoSomething();
        }
	} 
}