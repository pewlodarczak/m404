public class BasicTypes
{
    public void PrintTypes()
    {
        Console.WriteLine("Values:\n");
        int value = 8;
        Console.WriteLine("int value: " + value);
        //value = 8.7; // ERROR, wrong data type
        float fValue = (float)18.963; // float with a cast
        Console.WriteLine("float value: " + fValue);
        float fValue2 = 27.534f; // float with a cast
        Console.WriteLine("float value: " + fValue2);
        double dValue = 324574.55434667;
        Console.WriteLine("double value: " + dValue);
        dValue = 25433.063456; // new double value assigned
        Console.WriteLine("double value: " + dValue);
        char chr = 'z';
        Console.WriteLine("char value: " + chr);
        chr = (char)123; // Interpreted as ASCII char code
        Console.WriteLine("char value: " + chr);
        float fVal = (float)18.234;
    }
}