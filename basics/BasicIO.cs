class BasicIO
{
    public void GetName()
    {
        Console.WriteLine("Enter your name:");
        string userName = Console.ReadLine(); // read user input as string
        Console.WriteLine("Your name: " + userName);
        Console.WriteLine("Enter your age:");
        int age = Convert.ToInt32(Console.ReadLine()); // read user input as number
        Console.WriteLine("Your age: " + age);
        if(age > 20)
        {
            Console.WriteLine("Older than 20 " + true);
        }
        Console.WriteLine("2 == 2 " + (2 == 2));
        Console.WriteLine("2 > 2 " + (2 > 2));
    }
}