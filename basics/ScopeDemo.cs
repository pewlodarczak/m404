public class Scope
{
    int x; // can be accessed by all methods inside the class
    public int w; // can be accessed by the world
    
	public void ScopeMethod() {
	    int z = 10; // can only be accessed inside the ScopeMethod() method
	    x = 2;
		Calc();
	}
	
	private void Calc() {
	    int y = 0; // can only be accessed inside the Calc() method
	    y++;
	    {
	        int z = 20;
	        Console.WriteLine("z " + z);
	    }
	    Console.WriteLine("x " + x);
	    Console.WriteLine("y " + y);
	    //z++; // ERROR, cannot be accessed
	}
}

//using NamespaceName;
