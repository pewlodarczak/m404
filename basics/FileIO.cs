

class TextFileIO
{
    public void WriteToFile(string FilePath, string Line)
    {
        if (!File.Exists(FilePath))
        {
            using (StreamWriter sw = File.CreateText(FilePath))
            {
                sw.WriteLine(Line);
            }
        }
        else
        {
            AppendToFile(FilePath, Line);
        }
    }

    public void ReadFile(string FilePath)
    {
        using (StreamReader sr = File.OpenText(FilePath))
        {
            string s = "";
            while ((s = sr.ReadLine()) != null)
            {
                Console.WriteLine(s);
            }
        }
    }

    public void AppendToFile(string FilePath, string Line)
    {
        using (StreamWriter sw = File.AppendText(FilePath))
        {
            sw.WriteLine(Line);
        }
    }

    public void DeleteLine(string FilePath)
    {
        File.WriteAllLines(FilePath, File.ReadLines(FilePath).Where(line => line != "RemoveMe").ToList());
    }

}