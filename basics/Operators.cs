class Operators
{
    public void BasicOperators()
    {
        int a = 4;
        int b = 5;
        bool result;
        result = a < b; // true
        Console.WriteLine(result);
        result = a > b; // false
        Console.WriteLine(result);
        result = a <= 4; // a smaller or equal to 4 - true
        result = b >= 6; // b bigger or equal to 6 - false
        result = a == b; // a equal to b - false
        result = a != b; // a is not equal to b - true
        result = a > b || a < b; // Logical or - true
        result = 3 < a && a < 6; // Logical and - true
        result = !result; // Logical not - false
    }

    public void LogicalOperators()
    {
        int x = 4;
        Console.WriteLine(x < 5 && x < 10); // true
        Console.WriteLine(x < 5 || x < 4); // true
        Console.WriteLine(!(x < 5 && x < 10)); // false
        Console.WriteLine(x < 5 && x < 10 && x > 7); // false
    }

    public void AssignmentOperators()
    {
        int a = 5; // assignment
        int b = 3;
        b += a; // equal to b = b + a 
        Console.WriteLine(a += 7); // equal to a = a + 7 
        Console.WriteLine(a -= 3); // equal to a = a - 3
        Console.WriteLine(a *= 2); // equal to a = a * 2
        Console.WriteLine(a /= 3); // equal to a = a / 3
    }

    public void TernaryOperator()
    {
        int a, b;
        a = 10;
        b = (a == 1) ? 20 : 30; // false
        Console.WriteLine("Value of b is : " + b);
        b = (a == 10) ? 20 : 30; // true
        Console.WriteLine("Value of b is : " + b);
    }
}


