class Loops
{
    public void Loop()
    {
        int i = 10;
        while(i < 20) // condition must be true 
        {
            Console.WriteLine("Value " + i);
            i++;
        }

    }

    public void ForLoop()
    {
        for(int x = 10; x < 20; x = x + 1)
        {
            Console.WriteLine("Value : " + x );
        }
    }

    public void DoWhile()
    {
        int x = 10;
        do
        {
            Console.WriteLine("Value: " + x);
            x++;
        } while (x < 20);
    }

    public void ForeachLoop()
    {
        int []iArray = {3, 5, 2, 8, 2};
        foreach(int i in iArray)
            Console.WriteLine("Elem: " + i);
    }

    public int Sum(int i)
    {
        while(i > 0) // halting condition
            return i + Sum(i -1); // recursion
        return 0;
    }
}