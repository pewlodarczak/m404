public class AgeChecker
{
	public void CheckAge() {
		float age;
		Console.WriteLine("Enter your age:");
		age = Single.Parse(Console.ReadLine()); // read float input
        Console.WriteLine("Your age = {0}", age);
		if(age < 18)
		    Console.WriteLine("Sorry no alcohol");
	}
}

public class PositiveOrNegative
{
	public void PositiveOrNegativeNum() 
    {
		double number;
		Console.WriteLine("Enter a number:");
		number = double.Parse(Console.ReadLine());
		
		// positive or negative number?
		if(number > 0) {
		    Console.WriteLine(number + " is positive");
		} else if(number < 0) {
		    Console.WriteLine(number + " is negative");
		} else
    		Console.WriteLine("Number is 0");
	}
}

public class Menu
{
    public void ShowMenu()
    {
        Console.WriteLine("Enter your choice:");
        Console.WriteLine("c\tConfluence");
        Console.WriteLine("j\tJira");
        char c = Console.ReadKey().KeyChar;
        switch(c)
        {
            case 'c':
                Console.WriteLine("\nStarting Confluence");
                break;
            case 'j':
                Console.WriteLine("\nStarting Jira");
                break;
            default:
                Console.WriteLine("\nWrong selection");
                break;
        }
    }
}
