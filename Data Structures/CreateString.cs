class CreateString
{
    public void CreateAString() {
		String message = "What's up, doc?";
		Console.WriteLine(message);
		char[] msgArray = {'W', 'h', 'a', 't', '\'', 's', ' ', 'u', 'p', ',', ' ', 'd', 'o', 'c', '?'};
		// create String with char array
		message = new String(msgArray);
		Console.WriteLine(msgArray);
		// use quotes within String
		message = "Any man who must say \"I am the king\" is no true king.";
		Console.WriteLine(message);
		// each word on a new line
		message = "Eat\nmy\nshorts";
		Console.WriteLine(message);
		// tabs between words
		message = "Live\tlong\tand\tprosper";
		Console.WriteLine(message);
		String str1 = "Good", str2 = "news", str3 = "everyone";
		// concatenate strings
		Console.WriteLine(str1 + " " + str2 + " " + str3);
	}
}