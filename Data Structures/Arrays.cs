
class ArrayDemo
{
    float[] markList = { 5.5f, 6f, 5.25f, 5.75f };
    public void MarkArray()
    {
        // Print all the array elements
        for (int i = 0; i < markList.Length; i++)
        {
            Console.WriteLine(markList[i] + " ");
        }

        // Finding the average
        double average = 0;
        for (int i = 0; i < markList.Length; i++)
        {
            average += markList[i];
        }
        average /= markList.Length;
        Console.WriteLine("Average is " + average);

        // Finding the best mark
        double max = markList[0];
        for (int i = 1; i < markList.Length; i++)
        {
            if (markList[i] > max) max = markList[i];
        }
        Console.WriteLine("Best mark is " + max);
    }

    public void LoopThroughArray()
    {
        // Print all the array elements
        foreach (float mark in markList)
        {
            Console.WriteLine(mark);
        }
    }

    public void ArrayAsArgument()
    {
        char[] charArray = { 'R', 'e', 't', 'u', 'r', 'n', ' ', 'o', 'f', ' ', 't', 'h', 'e', ' ', 'J', 'e', 'd', 'i' };
        printArray(charArray);

        Console.WriteLine("\n");

        for (int i = 0; i < getArray().Length; i++)
        {
            if (i % 2 == 0)
            {
                Console.Write(getArray()[i]);
                Console.Write(" ");
            }
        }
        Console.Write("\n");
        Console.Write(" ");
        for (int i = 0; i < getArray().Length; i++)
        {
            if (i % 2 > 0)
            {
                Console.Write(getArray()[i]);
                Console.Write(" ");
            }
        }
        Console.Write("\n");
        for (int i = 0; i < getArray().Length; i++)
        {
            if (i % 2 == 0)
            {
                Console.Write(getArray()[i]);
                Console.Write(" ");
            }
        }
    }

    private void printArray(char[] charArray)
    {
        foreach (char aChar in charArray)
        {
            Console.Write(aChar);
            try
            {
                Thread.Sleep(10);
            }
            catch (Exception e)
            {
                // bad exception handling
            }
        }
    }

    private int[] getArray()
    {
        int[] intList = { 9, 6, 9, 6, 9, 6, 9, 6, 9, 6 };
        return intList;
    }

    public void TwoDArray()
    {
        char[,] cArr = new char[2,3]{{'a', 'b', 'c'}, {'c', 'b', 'a'}};
		for(int row = 0; row < 2; row++) {
		    for(int column = 0; column < 3; column++) {
		        Console.WriteLine("Val at row " + row + " column " + column + " " + cArr[row, column]);
		    }
		}
    }
}