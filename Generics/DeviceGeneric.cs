public class DeviceGeneric<T> where T : Device
{

    private T device;

    public T Device
    {
        get {return device;}
        set {device = value;}
    }

    public bool IsOn()
    {
        return device.IsSwitchedOn;
    }
}