public class MathOperations<T>
{
    private T data;
    public T Data {
        get {return data;}
        set{data = value;}
    }

    public T Bigger<T>(T input1, T input2)
    {
        return Comparer<T>.Default.Compare(input1, input2) > 0 ? input1 : input2;
    }

    public T Max<T>(T input1, T input2) where T : IComparable<T>
    {
        return input1.CompareTo(input2) > 0 ? input1 : input2;
    }

}