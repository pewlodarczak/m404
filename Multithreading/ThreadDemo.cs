using System.Threading;

class ThreadDemo
{
    // shared memory variable between the two threads  
    // used to indicate which thread we are in  
    private string _threadOutput = "";
    private bool _stopThreads;

    /// <summary>  
    /// Thread 1: Loop continuously,  
    /// Thread 1: Displays that we are in thread 1  
    /// </summary>  
    public void DisplayThread1()
    {
        while (_stopThreads == false)
        {
            lock(this)
            {
                Console.WriteLine("Display Thread 1");
                // Assign the shared memory to a message about thread #1  
                _threadOutput = "Hello Thread1";
                Thread.Sleep(1000);  // simulate a lot of processing   

                // tell the user what thread we are in thread #1, and display shared memory  
                Console.WriteLine("Thread 1 Output --> {0}", _threadOutput);
            }
        }
    }

    /// <summary>  
    /// Thread 2: Loop continuously,  
    /// Thread 2: Displays that we are in thread 2  
    /// </summary>  
    public void DisplayThread2()
    {
        while (_stopThreads == false)
        {
            lock(this)
            {
                Console.WriteLine("Display Thread 2");

                // Assign the shared memory to a message about thread #2  
                _threadOutput = "Hello Thread2";
                Thread.Sleep(1000);  // simulate a lot of processing  

                // tell the user we are in thread #2  
                Console.WriteLine("Thread 2 Output --> {0}", _threadOutput);
            }
        }
    }
}
