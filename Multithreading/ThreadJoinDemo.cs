using System.Threading;

class ThreadJoinDemo
{
    private bool _stopThreads;
    private string _threadOutput = "";
    
    public void DisplayThread()
    {
        while (_stopThreads == false)
        {
            //Console.WriteLine("Display Worker Thread");
            _threadOutput = "Worker Thread";
            Thread.Sleep(3000);
            Console.WriteLine("Worker Thread --> {0}", _threadOutput);
        }
    }
}
