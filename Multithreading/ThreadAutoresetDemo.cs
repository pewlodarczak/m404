using System.Threading;

class ThreadAutoresetDemo
{
    AutoResetEvent _blockThread1 = new AutoResetEvent(false);
    AutoResetEvent _blockThread2 = new AutoResetEvent(true);

    // shared memory variable between the two threads  
    // used to indicate which thread we are in  
    private string _threadOutput = "";
    private bool _stopThreads;

    /// <summary>  
    /// Thread 1: Loop continuously,  
    /// Thread 1: Displays that we are in thread 1  
    /// </summary>  
    public void DisplayThread1()
    {
        while (_stopThreads == false)
        {
            // block thread 1  while the thread 2 is executing
            _blockThread1.WaitOne();
            Console.WriteLine("Display ARThread 1");
            // Assign the shared memory to a message about thread #1  
            _threadOutput = "Hello ARThread1";
            Thread.Sleep(1000);  // simulate a lot of processing   

            // tell the user what thread we are in thread #1, and display shared memory  
            Console.WriteLine("ARThread 1 Output --> {0}", _threadOutput);
            // finished executing the code in thread 1, so unblock thread 2
            _blockThread2.Set();
        }
    }

    /// <summary>  
    /// Thread 2: Loop continuously,  
    /// Thread 2: Displays that we are in thread 2  
    /// </summary>  
    public void DisplayThread2()
    {
        while (_stopThreads == false)
        {
            _blockThread2.WaitOne();
            Console.WriteLine("Display ARThread 2");

            // Assign the shared memory to a message about thread #2  
            _threadOutput = "Hello ARThread2";
            Thread.Sleep(1000);  // simulate a lot of processing  

            // tell the user we are in thread #2  
            Console.WriteLine("ARThread 2 Output --> {0}", _threadOutput);
            _blockThread1.Set();
        }
    }
}
