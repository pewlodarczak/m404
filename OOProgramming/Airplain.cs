public abstract class Airplane
{
    //protected float _MaxSpeed;

    public abstract float MaxSpeed
    {
        get;
        set;
    }

    public abstract int NumOfPassengers();

    public int NumOfPilots()
    {
        return 2;
    }

    public virtual string PropulsionType()
    {
        return "Engine";
    }
}

class PropellerPlane : Airplane
{
    private float _MaxSpeed = 975f;
    private int _Passengers = 60;

    public override float MaxSpeed { get => _MaxSpeed; set => value = _MaxSpeed; }

    public override int NumOfPassengers()
    {
        return _Passengers;
    }

    public override string PropulsionType()
    {
        return "Propeller";
    }
}

class Jet : Airplane
{
    private float _MaxSpeed = 2179f;
    public override float MaxSpeed { get => _MaxSpeed; set => value = _MaxSpeed; }

    private int _Passengers = 185;

    public override int NumOfPassengers()
    {
        return _Passengers;
    }

    public override string PropulsionType()
    {
        return "Jet propulsion";
    }

}