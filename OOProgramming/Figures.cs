abstract class Figures
{
    public abstract void Draw();
    public virtual void DrawGeneric()
    {
        Console.WriteLine("\n");
        Console.WriteLine("\t  @..@");
        Console.WriteLine("\t (----)");
        Console.WriteLine("\t( >__< )");
        Console.WriteLine("\t^^ ~~ ^^");
        Console.WriteLine("\n");
    }
}

class Rectangle : Figures
{
    public override void Draw()
    {
        Console.Write("\n\n");
        Console.Write(@"#####################
#                   #
#                   #
#####################");
        Console.Write("\n\n");
    }

}

class Rabit : Figures
{
    public override void Draw()
    {
        char[,] rabit = {{'(', '\\', '_', '_', '_', '/', ')'},
                          {'(', '=', '^', '.', '^', '=', ')'},
                          {'(', '"', ')', '_', '(', '"', ')'}};
        for(int r = 0; r < 3; r++) {
            Console.Write('\t');
            for(int c = 0; c < 7; c++) {
                Console.Write(rabit[r,c]);
            }
            Console.Write('\n');
        }
    }

    public override void DrawGeneric()
    {
        Console.WriteLine("                           (o)(o)");
        Console.WriteLine("                          /      \\");
        Console.WriteLine("                         /       |");
        Console.WriteLine("                        /   \\  * |");
        Console.WriteLine("          ________     /    /\\__/");
        Console.WriteLine("  _      /        \\   /    /");
        Console.WriteLine(" / \\    /  ____    \\_/    /");
        Console.WriteLine("//\\ \\  /  /    \\         /");
        Console.WriteLine("V  \\ \\/  /      \\       /");
        Console.WriteLine("    \\___/        \\_____/");
        Console.WriteLine("\n");
    }

}

class FigStore<T> where T : Figures
{
    private List<T> figStore = new List<T>();

    public void Add(T figure)
    {
        figStore.Add(figure);
    }

    public List<T> GetAll()
    {
        return figStore;
    }

}