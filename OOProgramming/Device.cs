interface IDevice
{
    public bool SwitchOn();
    public bool IsSwitchedOn
    {
        get;
        set;
    }
}

public class Device : IDevice
{
    private bool SwitchedOn = false;

    public bool IsSwitchedOn { get => SwitchedOn; set => SwitchedOn = value; }

    public virtual bool SwitchOn()
    {
        IsSwitchedOn = true;
        Console.WriteLine("Device Switched on");
        return IsSwitchedOn;
    }

    //public abstract float Measure();
}

class TemperatureSensor : Device
{
    public override bool SwitchOn()
    {
        IsSwitchedOn = true;
        Console.WriteLine("Temperature Sensor Switched on");
        return IsSwitchedOn;
    }
}

class HumiditySensor : Device
{
    public override bool SwitchOn()
    {
        IsSwitchedOn = true;
        Console.WriteLine("Humidity Sensor Switched on");
        return IsSwitchedOn;
    }
}