namespace InstrumentsOrig;

interface IInstrument
{
    void PlaySound();
}

class Instrument : IInstrument
{

    public virtual void PlaySound()
    {
        Console.WriteLine("Sound");
    }
}

interface IStringInstrument
{
    bool IsBowed
    {
        get;
    }
}

class StringInstrument : Instrument, IStringInstrument
{
    private bool _Bowed = true;
    public virtual bool IsBowed
    {
        get => _Bowed;
    }
    public override void PlaySound()
    {
        Console.WriteLine("String sound");
    }

    protected int NumOfStrings()
    {
        return 4;
    }
}

class Violin : StringInstrument
{
    public override void PlaySound()
    {
        Console.WriteLine("Bowed sound, number of strings " + NumOfStrings());
    }
}

class Guitar : StringInstrument
{
    bool _Bowed = false;
    public override bool IsBowed
    {
        get => _Bowed;
    }

    public override void PlaySound()
    {
        Console.WriteLine("Plucked sound, number of strings " + NumOfStrings());
    }

}

class InstrInventory
{
    List<IStringInstrument> strInv = new List<IStringInstrument>();

    public void Add(IStringInstrument StrInst)
    {
        strInv.Add(StrInst);
    }

    public void ListInventory()
    {
        foreach(StringInstrument str in strInv)
        {
            Console.WriteLine(str.GetType() + " " + str.GetType().BaseType + " " + (str.IsBowed ? "bowed" : "plucked"));
        }
    }
}