namespace InstrumentWebshop;

public class Aufgabe11 
{
    public void Main()
    {
        Instrument inst = new();
        inst.PlaySound();
    }
}

//Interfaces und die Methoden darin sind immer public
internal interface I_Instrument 
{ 
    //Properties
    string Type {get;set;}

    //Methods
    void PlaySound();
}

public class Instrument : I_Instrument
{
    //Field
    private string? type;

    //Properties
    public string Type {get => type; set => type = value;}

    //Methods
    public void PlaySound()
    {
        Console.WriteLine("Playing sound");
    }
}

class Inventar
{
    //Field
    List<I_Instrument> inventory = new List<I_Instrument>();

    //Properties

    //Methods
    public void AddInstrument(I_Instrument inst)
    {
        inventory.Add(inst);
    }
}