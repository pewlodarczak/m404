namespace Instruments;

interface IInstrument
{
    void PlaySound();
}
class Instrument : IInstrument
{
    private bool _Type;

    public virtual bool Type {get => _Type;set => _Type = value;}
    public virtual void PlaySound()
    {
        Console.WriteLine("Play sound");
    }
}

class KeyInstrument : Instrument
{

    override public void PlaySound()
    {
        Console.WriteLine("Key sound");
    }
}

class Piano : KeyInstrument
{

}

interface IStringInstrument
{
    bool IsBowed
    {
        get;
    }
}

class StringInstrument : Instrument, IStringInstrument
{

    private bool _Bowed = true;
    public virtual bool IsBowed
    {
        get => _Bowed;
    }

    override public void PlaySound()
    {
        Console.WriteLine("Bowed sound");
    }

    public virtual void Strings()
    {
        Console.WriteLine("1 string");
    }

}

class Violin : StringInstrument
{
    override public void Strings()
    {
        Console.WriteLine("4 strings");
    }

}

class Guitar : StringInstrument
{
    bool _Bowed = false;
    public override bool IsBowed
    {
        get => _Bowed;
    }

    override public void PlaySound()
    {
        Console.WriteLine("Plucked sound");
    }
}

class Harp : StringInstrument
{
    bool _Bowed = false;
    public override bool IsBowed
    {
        get => _Bowed;
    }

    override public void PlaySound()
    {
        Console.WriteLine("Plucked sound");
    }

    override public void Strings()
    {
        Console.WriteLine("47 strings");
    }


}

class InstrInventory
{
    List<KeyInstrument> keyInvent = new List<KeyInstrument>();
    List<StringInstrument> strInvent = new List<StringInstrument>();

    public void AddKeyInstrument(KeyInstrument inst)
    {
        keyInvent.Add(inst);
    }

    public void AddStrInstrument(StringInstrument inst)
    {
        strInvent.Add(inst);
    }

    public void ListIntruments()
    {
        foreach(KeyInstrument i in keyInvent)
        {
            Console.WriteLine(i.GetType());
            i.PlaySound();
        }

        foreach(StringInstrument i in strInvent)
        {
            Console.WriteLine(i.GetType() + " " + i.GetType().BaseType + " " + (i.IsBowed ? "bowed" : "plucked"));
            i.PlaySound();
            i.Strings();
        }

    }
}