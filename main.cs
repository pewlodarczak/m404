using Room;
using IoT;
using NamespaceName2;
using FileIO;
using Planetarium;
using Instruments;

class MainHeatProgramm
{
    static void Main(String[] args)
    {
        //new BasicIO().GetName();
        //new AgeChecker().CheckAge();
        //new PositiveOrNegative().PositiveOrNegativeNum();
        //new Menu().ShowMenu();
        //new Loops().Loop();
        //new Loops().ForLoop();
        //new Loops().DoWhile();
        //new Loops().ForeachLoop();
        //int i = 10;
        //Console.WriteLine(new Loops().Sum(i));
        //new ArrayDemo().MarkArray();
        //new ArrayDemo().LoopThroughArray();
        //new ArrayDemo().ArrayAsArgument();
        //new ArrayDemo().TwoDArray();
        //new CreateString().CreateAString();
        //new Scope().ScopeMethod();
        //new ClassName().DoSomething();
        /******** OO Programming *******
        Device device = new Device();
        device.SwitchOn();
        Device temperatureSensor = new TemperatureSensor();
        temperatureSensor.SwitchOn();
        //Console.WriteLine("Switched on: " + temperatureSensor.IsSwitchedOn);
        Device humiditySensor = new HumiditySensor();
        humiditySensor.SwitchOn();
        */
        /************* Planets
                var earth = Earth.Instance;
                var earth2 = Earth.Instance;
                Console.WriteLine(earth.GetHashCode());
                Console.WriteLine(earth2.GetHashCode());
                earth.Distance = 149.6;
                earth.Speed = 29.8;
                earth.Diam = 12756;
                var venus = Venus.Instance;
                venus.Distance = 108.2;
                venus.Speed = 35.0;
                venus.Diam = 12104;
                var jupiter = Jupiter.Instance;
                jupiter.Distance = 778.5;
                jupiter.Speed = 13.1;
                jupiter.Diam = 142984;

                Console.WriteLine("Planet\t\tDistance from sun (10^6km)\tOrbital Speed (km/s)\tDiameter (km)");
                Console.WriteLine($"Earth:\t\t{earth.Distance}\t\t\t\t{earth.Speed}\t\t\t{earth.Diam}");
                Console.WriteLine($"Venus:\t\t{venus.Distance}\t\t\t\t{venus.Speed}\t\t\t{venus.Diam}");
                Console.WriteLine($"Jupiter:\t{jupiter.Distance}\t\t\t\t{jupiter.Speed}\t\t\t{jupiter.Diam}");

                Console.WriteLine("\n\n");
                Console.WriteLine($"Jupiter {jupiter.GetType()}");
                Console.WriteLine($"Earth {earth.GetType()}");

                Console.WriteLine($"\nEarth can be landed {earth.Land : \"yes\" : \"no\"}");
                Console.WriteLine($"Venus can be landed {venus.Land : \"yes\" : \"no\"}");
                Console.WriteLine($"Jupiter can be landed {jupiter.Land : \"yes\" : \"no\"}");

                var pl = new Planet();
                Planet.Do();
                Console.WriteLine(pl.Distance);
                Earth.Do();

                var planetInventory = new PlanetInventory();
                planetInventory.ListPlanets();

                Console.WriteLine("\n");
                foreach(Planet p in planetInventory)
                {
                    Console.WriteLine($"Planet {p.GetType()}");
                }

        *************/
        /*
                string FilePath = "C:\\Users\\Peter\\projects\\csaufgaben\\Data\\PlainText.txt";
                string Line = "Life is never fair, and perhaps it is a good thing for most of us that it is not. - Oscar Wilde" + Environment.NewLine;
                var textFileIO = new TextFileIO();
                textFileIO.WriteToFile(FilePath, Line);
                textFileIO.ReadFile(FilePath);
                textFileIO.DeleteLine(FilePath);

        /*
                MathOperations<int> mathIntOperations = new MathOperations<int>();
                mathIntOperations.Data = 99;
                Console.WriteLine("Data: " + mathIntOperations.Data);
                Console.WriteLine("Bigger value: " + mathIntOperations.Bigger(9, 5));
                Console.WriteLine("Max value: " + mathIntOperations.Max(9, 5));

                MathOperations<float> mathFloatOperations = new MathOperations<float>();
                mathFloatOperations.Data = 99.99F;
                Console.WriteLine("Data: " + mathFloatOperations.Data);
                Console.WriteLine("Bigger value: " + mathFloatOperations.Bigger(9.7, 5.2));
                Console.WriteLine("Max value: " + mathFloatOperations.Max(9.7, 5.2));

                var temperatureSensor = new TemperatureSensor();
                temperatureSensor.SwitchOn();
                var humiditySensor = new HumiditySensor();
                humiditySensor.SwitchOn();
                DeviceGeneric<Device> deviceGeneric = new DeviceGeneric<Device>();
                deviceGeneric.Device = temperatureSensor;
                Console.WriteLine("Switched on: " + deviceGeneric.IsOn());
*/

                DataStore<int> dsInt = new DataStore<int>();
                dsInt.AddOrUpdate(3, 33);
                dsInt.AddOrUpdate(5,55);
                Console.WriteLine(dsInt.GetData(3));
                Console.WriteLine(dsInt.GetData(4));

                DataStore<string> dsStr = new DataStore<string>();
                dsStr.AddOrUpdate(2, "hello");
                dsStr.AddOrUpdate(5,"world");
                Console.WriteLine(dsStr.GetData(2));
                Console.WriteLine(dsStr.GetData(4));
                

        /*
                BasicClass basicClass = new BasicClass(); // create object of type BasicClass
                int objVar = basicClass.GetVariable(); // call member method
                Console.WriteLine("Var: " + objVar);

                Student student1 = new Student("Jane Millet");
                Console.WriteLine("Student 1: " + student1.GetStudentName());
                Student student2 = new Student("John Smith");
                Console.WriteLine("Student 2: " + student2.GetStudentName());

        /*
                Blinds blinds1 = new Blinds("1");
                blinds1.closeBlinds();
                blinds1.openBlinds();

                Blinds blinds2 = new Blinds("2");
                blinds2.closeBlinds();
                blinds2.openBlinds();

                AirConditioning airConditioning = new AirConditioning();
                airConditioning.turnOn();
                AirConditioning.GetObjType();
        */
        /*
        BasicIO bio = new BasicIO();
        bio.getName();
        */
        //startAutomation();
        /*
                int[] iArr = {3, 5, 8, 10};

                int max_room = 100;
                HeatSystem [] hsArr = new HeatSystem[max_room];

                for(int i = max_room -1; i >= 0; i--) {
                    hsArr[i] = new HeatSystem("Room " + i + " HS");
                }

                foreach(HeatSystem h in hsArr) {
                    Console.WriteLine("Room: " + h.GetRoom());
                }
        */
        /*
        Console.WriteLine("Enter temperature:");
        float temp = (float)Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Temperature set: " + temp);
        hs1.setTemp(temp);
        Console.WriteLine("Temp is " + hs1.getTemp());
    */

        /****************** FileIO ******************/
        /*string FilePath = ".\\Data\\TextOutput.txt";
        var file = new ReadWriteFile(FilePath);
        file.WriteToFile("gobbledygook");

        file.FilePath = ".\\Data\\TextOutput1.txt";
        
        file.WriteToFile("Hello C#");
        
        file.FilePath = ".\\Data\\TextOutput2.txt";
        file.WriteStream("Explore, analyze, and share quality data");
        file.FilePath = ".\\Data\\TextOutput1.txt";
        file.ReadStream();
        */

        // StringInstrument violin = new Violin();
        // violin.PlaySound();
        // StringInstrument guitar = new Guitar();
        // guitar.PlaySound();

        // var inv = new InstrInventory();
        // inv.Add(violin);
        // inv.Add(guitar);
        // inv.ListInventory();

        // IInstrument inst = new Instrument();
        // var invent = new InstrInventory();

        // var piano1 = new Piano();
        // var piano2 = new Piano();
        // var violin = new Violin();
        // var guitar = new Guitar();
        // //piano1.PlaySound();
        // invent.AddKeyInstrument(piano1);
        // invent.AddKeyInstrument(piano2);
        // invent.AddStrInstrument(violin);
        // invent.AddStrInstrument(guitar);
        // //invent.ListIntruments();

        // var harp = new Harp();
        // invent.AddStrInstrument(harp);
        // invent.ListIntruments();

        var threadDemo = new ThreadDemo();
        // construct two threads for our demonstration;  
        Thread thread1 = new Thread(new ThreadStart(threadDemo.DisplayThread1));
        Thread thread2 = new Thread(new ThreadStart(threadDemo.DisplayThread2));

        // start them  
        // thread1.Start();
        // thread2.Start();
        // Thread.Sleep(5000);
        // thread1.Interrupt();
        // thread2.Interrupt();

        // Console.WriteLine("ThreadARDemo");
        // var threadARDemo = new ThreadAutoresetDemo();
        // Thread threadAR1 = new Thread(new ThreadStart(threadARDemo.DisplayThread1));
        // Thread threadAR2 = new Thread(new ThreadStart(threadARDemo.DisplayThread2));

        // threadAR1.Start();
        // threadAR2.Start();

        // Thread.Sleep(5000);
        // threadAR1.Interrupt();
        // threadAR2.Interrupt();
/*
        Thread th = Thread.CurrentThread;
        th.Name = "Main Thread";
        var threadJoinDemo = new ThreadJoinDemo();
        //Thread threadJoin = new Thread(new ThreadStart(threadJoinDemo.DisplayThread));
        Thread threadJoin = new Thread(threadJoinDemo.DisplayThread);
        threadJoin.Start();
        //Using Join to block the current Thread
        threadJoin.Join();
        Thread.Sleep(1000);
        Console.WriteLine($"Thread {th.Name}");
        Console.WriteLine($"Main Thread Priority: {th.Priority}");
*/
        // Delegates
        // DelegateDemo dd = new();
        // ADelegate del = dd.PrintMessage;
        // del("The cat and the fiddle");
        // dd.InvokeDelegate(del);
        // del = dd.PrintWords;
        // dd.InvokeDelegate(del);
        // ADelegate newdel = (string msg) =>  {Console.WriteLine($"1. {msg}");
        //                                      Console.WriteLine($"2. {msg}");};
        // newdel("The cow jumped over the moon");

        Airplane pPlane = new PropellerPlane();
        Airplane jPlane = new Jet();

        List<Airplane> planeList = new List<Airplane>();
        planeList.Add(pPlane);
        planeList.Add(jPlane);
        foreach(Airplane ap in planeList)
        {
            Console.WriteLine(ap.PropulsionType());
            Console.WriteLine(ap.MaxSpeed);
            Console.WriteLine(ap.NumOfPassengers());
            Console.WriteLine(ap.NumOfPilots());
        }

        var jPlane2 = new Jet();
        Console.WriteLine(jPlane2.PropulsionType());

        var gds = new DataStore<Airplane>();
        gds.AddOrUpdate(0, jPlane2);
        Console.WriteLine(gds.GetData(0));

        // Figures[] fig = {new Rectangle(), new Rabit()};
        // foreach(Figures f in fig)
        // {
        //     //f.Draw();
        //     f.DrawGeneric();
        // }
        
        var fs = new FigStore<Figures>();
        fs.Add(new Rectangle());
        fs.Add(new Rabit());
        foreach(Figures f in fs.GetAll())
        {
            f.Draw();
            //f.DrawGeneric();
        }
    }

    static void startAutomation()
    {
        DeviceList deviceList = DeviceList.Instance;
        Heating heatingRoom1 = new Heating("Heating Room 1");
        deviceList.AddHeating(heatingRoom1);
        Heating heatingRoom2 = new Heating("Heating Room 2");
        deviceList.AddHeating(heatingRoom2);
        deviceList.GetAllHeatings();
        float thresh = 24F;
        bool threshSet = heatingRoom1.setThreashold(24);
        bool heatRoom1On = false;
        if(threshSet)
        {
            if(heatingRoom1.getTemperature() < thresh)
            {
                heatRoom1On = heatingRoom1.switchOn();
                if(heatRoom1On)
                {
                    Console.WriteLine("Heating \"" + heatingRoom1.getLabel() + "\" is on");
                }
            }
        }
        deviceList.GetAllHeatingsStatus();
    }
}

class BasicClass // Calss name
{
    private int aVar = 10; // member attribute

    public int GetVariable() // member variable
    {
        return aVar;
    }
}

class Student
{
    private string StudentName;

    // Constructor with one argument
    public Student(string name)
    {
        StudentName = name;
    }

    // getter method to return the students name
    public string GetStudentName()
    {
        return StudentName;
    }
}
